<?php
namespace DiegoSorrentino\Library;

class Logging{
	private string $path = './log/';
	private string $filename = 'file.log';

	private ?string $filepath = null;

	private bool $directoryExists = false;

	function __construct(
			?string $path = null, 
			?string $filename = null, 
			?bool $prefixDay = null){

		if(!empty($path)) $this->path = realpath('./' . $path);
		if(!empty($filename)) $this->filename = $filename;
		if($prefixDay) $this->filename = sprintf("%s_-_%s", date('Y-m-d'), $this->filename);

		$this->directoryExists = $this->mkPath();
		$this->filepath = sprintf('%s/%s', $this->path, $this->filename);
		
		if($this->directoryExists){
			ini_set('error_log', $this->filepath);
			return;
		}

		ini_set('error_log', '/dev/stderr');
	}

	private function mkPath() :bool{
		if( ! file_exists($this->path) ){
			mkdir($this->path, 0755, true);
			//on error i'll exit in next check ( ! is_dir() )
		}

		if( ! is_dir($this->path)){
			error_log("[ERR] {$this->path} exists, NOT is_dir");
			return false;
		}

		if( ! is_writable($this->path)){
			error_log("[ERR] {$this->path} exists, is_dir but NOT is_writable");
			return false;
		}

		error_log("[INF] {$this->path} exists, is_dir and is_writable");
		return true;
	}
}

?>
